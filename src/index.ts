
let array: number[] = [];
const args = process.argv.slice(2);
args[0].split(",").forEach(element => array.push(Number(element)));
const longestSubstring = {
    index: 0,
    length: 0,
};

const longestSequence = (array: number[]) => {
    if (!array.includes(0)) {
        return 0
    }

    for (let i = 0; i < array.length; i++) {
        const newArray = [...array]
        newArray[i] = 1
        const x = newArray.join('').split('0')

        for (let index = 0; index < x.length; index++) {
            const item = x[index]
            if (item.length >= longestSubstring.length) {
                longestSubstring.index = i
                longestSubstring.length = item.length
            }
        }

    }
    console.log(longestSubstring.index, "is the index of the 0 to be replace");
    console.log(longestSubstring.length, "is the longest sequence of the number 1 ");
}

longestSequence(array);


